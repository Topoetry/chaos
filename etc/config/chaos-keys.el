;;; chaos-keys.el --- general bindings for my config   -*- no-byte-compile: t; lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'general)

(general-create-definer chaos/leader-def
  :keymaps 'override
  :prefix "SPC"
  :states '(normal motion emacs))

(general-def :states '(normal motion emacs)
  "SPC" nil
  "M-SPC" (general-key "SPC"))

(general-def :states '(insert)
  "M-SPC" (general-key "SPC" :state 'normal))


(general-define-key
 :keymaps '(minibuffer-local-map
            minibuffer-local-ns-map
            minibuffer-local-completion-map
            minibuffer-local-must-match-map
            minibuffer-local-isearch-map)
 [escape] 'minibuffer-keyboard-quit)


(general-def :states '(insert)
  "<f1> e" 'eval-expression)
;;; top
(chaos/leader-def
  "RET" '(consult-bookmark :wk "Jump to bookmark")
  "'" '(vertico-repeat :wk "Resume last search")
  "." '(find-file :wk "Find file")
  ":" '(execute-extended-command :wk "M-x")
  ";" '(pp-eval-expression :wk "Eval expression")
  "<" '(consult-buffer :wk "Switch buffer")
  "`" '(evil-switch-to-windows-last-buffer :wk "Switch to last buffer") )
;;; buffers
(chaos/leader-def
  :infix "b"
  "" '(:which-key "buffers")
  "[" 'previous-buffer
  "]" 'next-buffer
  "b" 'ido-switch-buffer
  "B" 'consult-buffer
  "c" '(clone-indirect-buffer :wk "Clone buffer")
  "C" '(clone-indirect-buffer-other-buffer :wk "Clone buffer other window")
  "d" '(kill-current-buffer :wk "Kill buffer")
  ;; "O" 'kill-other-buffers
  "i" 'ibuffer
  "j" 'next-buffer
  "k" 'previous-buffer
  "l" '(evil-switch-to-windows-last-buffer :wk "Switch to last buffer")
  "m" '(bookmark-set :wk "Set bookmark")
  "M" '(bookmark-delete :wk "Delete bookmark")
  "n" 'next-buffer
  "N" '(evil-buffer-new :wk "New empty buffer")
  "p" 'previous-buffer
  "r" 'revert-buffer
  "R" 'rename-buffer
  "s" 'save-buffer
  "S" '(evil-write-all :wk "Save all buffers")
  "x" '((lambda () (interactive) (switch-to-buffer "\*scratch\*"))
        :which-key "Switch to scratch buffer")
  "y" '(lambda ()(interactive)(clipboard-kill-ring-save (point-min) (point-max)) :wk "Yank buffer")
  "z" 'bury-buffer)

;;; code

(chaos/leader-def
  :infix "c"
  "" '(:wk "code")
  "c" '(compile :wk "Compile")
  "C" '(recompile :w " Recompile")
  "e" '((lambda ()
          (interactive)
          (call-interactively
           (if (use-region-p)
               #'+eval/region
             #'+eval/buffer)))
        :wk "Evaluate buffer/region")
  "w" 'delete-trailing-whitespace
  "W" 'delete-trailing-lines
  "x" '(chaos/diag :wk "List errors"))

;;; file

(chaos/leader-def
  :infix "f"
  "" '(:which-key "file")
  "d" 'dired
  "e" '((lambda ()(interactive)
          (chaos-find-file-recursive-in-dir user-emacs-directory))
        :wk "Find file in emacsd")
  "E" '((lambda ()(interactive)
          (dired user-emacs-directory))
        :wk "Find file in emacsd")
  "f" 'find-file
  "F" '((lambda ()(interactive)
          (chaos-find-file-recursive-in-dir default-directory))
        :wk "Find file from here (slow)")
  "l" 'locate
  "r" 'recentf
  "s" 'save-buffer
  "S" 'write-file
  "u" 'chaos/doas-find-file
  "U" 'chaos/doas-this-file)

;;; help

(chaos/leader-def
  :infix "h"
  "" '(:which-key "help")
  "RET" 'info-emacs-manual
  "'" 'describe-char
  "." 'display-local-help
  "4i" 'info-other-window
  "a" 'apropos
  "A" 'apropos-documentation
  "b" '(:which-key "bindings")
  "bb" 'describe-bindings
  "bf" 'which-key-show-full-keymap
  "bi" 'which-key-show-minor-mode-keymap
  "bk" 'which-key-show-keymap
  "bm" 'which-key-show-major-mode
  "bt" 'which-key-show-top-level
  "c" 'describe-key-briefly
  "C" 'describe-coding-system
  "e" 'view-echo-area-messages
  "F" 'describe-face
  "g" 'describe-gnu-project
  "i" 'info
  "I" 'describe-input-method
  "K" 'Info-goto-emacs-command-node
  "l" 'view-lossage
  "L" 'describe-language-environment
  "m" 'describe-mode
  "P" 'find-library
  "q" 'help-quit
  "r" '(:which-key "reload")
  "rr" 'chaos/reload-config-file
  "rt" 'chaos/tangle-config-file
  "R" 'info-display-manual
  "s" 'describe-syntax
  "S" 'info-lookup-symbol
  "t" 'load-theme
  "T" '(:which-key "profiler")
  "Ts" 'profiler-start
  "Te" 'profiler-stop
  "Tp" 'profiler-report
  "w" 'where-is)

;;; insert

(chaos/leader-def
  :infix "i"
  "" '(:wk "insert")
  "e" '(emoji-search :wk "emoji")
  "f" '((lambda () (interactive)
          (let ((path (or buffer-file-name default-directory)))
            (insert (file-name-nondirectory path))))
        :wk "Current file name")
  "F" '((lambda ()  (interactive)
          (let ((path (or buffer-file-name default-directory)))
            (insert (abbreviate-file-name path))))
        :wk "Current file path")
  "r" '(consult-register :wk "From evil register")
  "u" '(insert-char :wk "unicode")
  "y" '(consult-yank-pop :wk "From clipboard"))

;;; open

(chaos/leader-def
  :infix "o"
  "" '(:wk "open")
  "-" 'dired
  "b" '(browse-url-of-file :wk "Default browser")
  "f" '(chaos/empty-frame :wk "New frame")
  ;; "F" '(select-frame-by-name :wk "Select frame") ; TODO broken on wayland so :
  "F" 'tear-off-window)

;;; search

(chaos/leader-def
  :infix "s"
  "" '(:wk "search")
  "f" '(locate :wk "Locate file")
  ;; "i" 'imenu
  "l" '(link-hint-open-link :wk "Jump to visible link")
  "L" '(ffap-menu :wk "Jump to link")
  "m" '(bookmark-jump :wk "Jump to bookmark")
  "r" '(evil-show-marks :wk "Jump to mark"))

;;; toggle

(chaos/leader-def
  :infix "t"
  "" '(:wk "toggle")
  "c" '(display-fill-column-indicator-mode :wk "Fill colomn indicator")
  "F" '(toggle-frame-fullscreen :wk "Frame fullscreen")
  "l" '(line-number-mode :wk "Line numbers")
  "r" '(read-only-mode :wk "Read only")
  "v" '(visible-mode :wk "Visible mode")
  "W" '(whitespace-mode :wk "Whitespace indicator")
  "w" '(visual-line-mode :wk "Visible line wrapping"))

;;; windows

(chaos/leader-def
  :infix "w"
  "" '(:which-key "window")
  "+" 'evil-window-increase-height
  "-" 'evil-window-decrease-height
  ":" 'evil-ex
  "<" 'evil-window-decrease-width
  "=" 'balance-windows
  ">" 'evil-window-increase-width
  "_" 'evil-window-set-height
  "b" 'evil-window-bottom-right
  "c" 'evil-window-delete
  "d" 'evil-window-delete
  "f" 'ffap-other-window
  "gt" 'tab-bar-switch-to-next-tab
  "gT" 'tab-bar-switch-to-prev-tab
  "o" 'delete-other-windows
  "h" 'evil-window-left
  "H" 'evil-window-move-far-left
  "j" 'evil-window-down
  "J" 'evil-window-move-very-bottom
  "k" 'evil-window-up
  "K" 'evil-window-move-very-up
  "l" 'evil-window-right
  "L" 'evil-window-move-far-right
  "n" 'evil-window-ew
  "p" 'evil-window-mru
  "q" 'evil-quit
  "r" 'evil-window-rotate-downwards
  "R" 'evil-window-rotate-upwards
  "s" 'evil-window-split
  "v" 'evil-window-vsplit
  "w" 'evil-window-next
  "W" 'evil-window-prev
  "x" 'evil-window-exchange
  "|" 'evil-window-set-width)

;;; quit

(chaos/leader-def
  :infix "q"
  "" '(:which-key "quit/session")
  "d" 'chaos/restart-server
  "f" 'chaos/delete-frame-with-prompt
  "F" 'delete-frame
  "K" 'save-buffers-kill-emacs
  "q" 'save-buffers-kill-terminal
  "R" 'chaos/restart)

(provide 'chaos-keys)
;;; chaos-keys.el ends here
