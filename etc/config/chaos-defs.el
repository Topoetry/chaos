;;; chaos-defs.el --- var and fun for my config    -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(defvar chaos/bytecom '("chaos-defs.el" "chaos-modeline.el" "chaos-symbols.el"))

;;;###autoload
(defun chaos/restart-server ()
  "Restart the Emacs server."
  (interactive)
  (require 'server)
  (server-force-delete)
  (while (server-running-p)
    (sleep-for 1))
  (server-start))

;;;###autoload
(defun chaos/tangle-config-file()
  "Tangle our configuration file."
  (interactive)
  (require 'no-littering)
  (require 'org)
  (org-babel-tangle-file (concat user-emacs-directory "init.org") (concat user-emacs-directory "init.el"))
  (dolist (f chaos/bytecom)
    (byte-recompile-file (concat no-littering-etc-directory  "config/" f) 0)))

;;;###autoload
(defun chaos/reload-config-file()
  "Reload our configuration file."
  (interactive)
  (chaos/tangle-config-file)
  (load-file (concat user-emacs-directory "init.el")))


;;;###autoload
(defun chaos-find-file-in-dir (dir)
  "Switch to another file starting from `DIR'."
  (unless (file-directory-p dir)
    (error "Directory %S does not exist" dir))
  (unless (file-readable-p dir)
    (error "Directory %S isn't readable" dir))
  (let* ((file (read-file-name "Find file "  dir)))
    (if (string= file "")
        (user-error "You didn't specify the file")
      (find-file file))))

;;;###autoload
(defun chaos-find-file-recursive-in-dir (dir)
  "Search for a file recursively in `DIR'."
  (unless (file-directory-p dir)
    (error "Directory %S does not exist" dir))
  (unless (file-readable-p dir)
    (error "Directory %S isn't readable" dir))
  (require 'project)
  (let* ((all-files (directory-files-recursively dir "."))
         (file (funcall project-read-file-name-function "Find file" all-files
                        nil 'file-name-history )))
    (if (string= file "")
        (user-error "You didn't specify the file")
      (find-file file))))

;;;###autoload
(defun chaos-project-root (&optional dir)
  "Return the project root of `DIR' (defaults to `default-directory').
Returns nil if not in a project."
  (nth 2 (project-current nil dir)))

;;;###autoload
(defun chaos-project-root-p (&optional dir)
  "Return the project root of `DIR' (defaults to `default-directory').
Returns nil if not in a project."
  (and (chaos-project-root dir)
       t))

;;;###autoload
(defun chaos/restart ()
  "Restart Emacs (and the daemon, if active).

    Unlike `chaos/restart-and-restore', does not restart the current session."
  (interactive)
  (require 'restart-emacs)
  (restart-emacs))

;;;###autoload
(defun chaos/delete-frame-with-prompt ()
  "Delete the current frame, but ask for confirmation if it isn't empty."
  (interactive)
  (if (cdr (frame-list))
      (when (yes-or-no-p "Close frame?")
        (delete-frame))
    (save-buffers-kill-emacs)))

;;;###autoload
(defun chaos/save-and-kill-this-buffer ()
  "Save and kill the current buffer."
  (interactive)
  (save-buffer)
  (kill-this-buffer))

;;;###autoload
(defun minibuffer-keyboard-quit ()
  "Abort recursive edit.
In Delete Selection mode, if the mark is active, just deactivate it;
then it takes a second \\[keyboard-quit] to abort the minibuffer."
  (interactive)
  (if (and delete-selection-mode transient-mark-mode mark-active)
      (setq deactivate-mark  t)
    (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
    (abort-recursive-edit)))

;;;###autoload
(defun my/escape-key ()
  "Get out search results, exit visual and abort C-g style."
  (interactive)
  (evil-ex-nohighlight)
  (evil-force-normal-state)
  (keyboard-quit))

;;;###autoload
(defun chaos/diag (&rest arg)
  "Show diagnostics for current buffer."
  (interactive)
  (cond ((bound-and-true-p lsp-mode)
         (consult-lsp-diagnostics arg))
        ((bound-and-true-p flymake-mode)
         (consult-flymake)
         (flymake-show-buffer-diagnostics))))

;;;###autoload
(defun chaos/empty-frame ()
  "Open an new frame on the dashboard."
  (interactive)
  (switch-to-buffer-other-frame "*dashboard*"))

;;;###autoload
(defun chaos/center-buffer-toggle ()
  "Toggle centering the buffer."
  (interactive)
  (if visual-fill-column-center-text
      (setq-local visual-fill-column-center-text nil)
    (setq-local visual-fill-column-center-text t)))

;;;###autoload
(defun chaos-center-buffer ()
  "Center buffer or toggle its centerd position if ARG is t"
  (setq-local visual-fill-column-width 100)
  (setq-local visual-fill-column-center-text t)
  (visual-fill-column-mode 1))


;;;###autoload
(defun chaos--doas-file-path (file)
  "Open FILE as root."
  (let ((host (or (file-remote-p file 'host) "localhost")))
    (concat "/" (when (file-remote-p file)
                  (concat (file-remote-p file 'method) ":"
                          (if-let* ((user (file-remote-p file 'user)))
                              (concat user "@" host)
                            host)
                          "|"))
            "doas:root@" host
            ":" (or (file-remote-p file 'localname)
                    file))))

;;;###autoload
(defun chaos/doas-find-file (file)
  "Open FILE as root.
This will prompt you to save the current buffer."
  (interactive (list (read-file-name "Open file as root: ")
                     current-prefix-arg))
  (let ((auto-save-default nil)
        (remote-file-name-inhibit-auto-save t)
        (remote-file-name-inhibit-auto-save-visited t))
    (find-file (chaos--doas-file-path (expand-file-name file)))))

;;;###autoload
(defun chaos/doas-this-file ()
  "Open the current file as root."
  (interactive)
  (chaos/doas-find-file
   (or (buffer-file-name (buffer-base-buffer))
       (when (or (derived-mode-p 'dired-mode)
                 (derived-mode-p 'wdired-mode))
         default-directory)
       (user-error "Current buffer isn't visiting a file"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; org
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;###autoload
(defun chaos/open-org-directory ()
  "Open org directory."
  (interactive)
  (chaos-find-file-recursive-in-dir org-directory))

;;;###autoload
(defun +org/export-to-clipboard (backend)
  "Exports the current buffer/selection to the clipboard.

        Prompts for what BACKEND to use. See `org-export-backends' for options."
  (interactive
   (list (intern (completing-read "Export to: " (progn (require 'ox) org-export-backends)))))
  (require 'ox)
  (let* ((org-export-show-temporary-export-buffer nil)
         (buffer (org-export-to-buffer backend "*Formatted Copy*" nil nil t t)))
    (unwind-protect
        (with-current-buffer buffer
          (kill-new (buffer-string)))
      (kill-buffer buffer))))

;;;###autoload
(defun +org/return ()
  "Call `org-return' then indent (if `electric-indent-mode' is on)."
  (interactive)
  (require 'org)
  (org-return electric-indent-mode))

;;;###autoload
(defun +org/shift-return (&optional arg)
  "Insert a literal newline, or dwim in tables.
Executes `org-table-copy-down' if in table."
  (interactive "p")
  (require 'org-table)
  (if (org-at-table-p)
      (org-table-copy-down arg)
    (org-return nil arg)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; elisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Fontification
(defvar +emacs-lisp--face nil)
;;;###autoload
(defun +emacs-lisp-highlight-vars-and-faces (end)
  "Match defined variables and functions.

Functions are differentiated into special forms, built-in functions and
library/userland functions"
  (require 'advice)
  (catch 'matcher
    (while (re-search-forward "\\(?:\\sw\\|\\s_\\)+" end t)
      (let ((ppss (save-excursion (syntax-ppss))))
        (cond ((nth 3 ppss)  ; strings
               (search-forward "\"" end t))
              ((nth 4 ppss)  ; comments
               (forward-line +1))
              ((let ((symbol (intern-soft (match-string-no-properties 0))))
                 (and
                  (cond ((null symbol) nil)
                        ((eq symbol t) nil)
                        ((keywordp symbol) nil)
                        ((special-variable-p symbol)
                         (setq +emacs-lisp--face 'font-lock-variable-name-face))
                        ((and (fboundp symbol)
                              (eq (char-before (match-beginning 0)) ?\()
                              (not (memq (char-before (1- (match-beginning 0)))
                                         (list ?\' ?\`))))
                         (let ((unaliased (indirect-function symbol)))
                           (unless (or (macrop unaliased)
                                       (special-form-p unaliased))
                             (let (unadvised)
                               (while (not (eq (setq unadvised
                                                     (ad-get-orig-definition
                                                      unaliased))
                                               (setq unaliased (indirect-function
                                                                unadvised)))))
                               unaliased)
                             (setq +emacs-lisp--face
                                   (if (subrp unaliased)
                                       'font-lock-constant-face
                                     'font-lock-function-name-face))))))
                  (throw 'matcher t)))))))
    nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; terminals

;; ;;;###autoload
;; (defun chaos/unique-vterm ()
;;   "Open vterm in a unique buffer."
;;   (interactive)
;;   (call-interactively 'vterm)
;;   (rename-uniquely))

;; ;;;###autoload
;; (defun chaos/vterm-popup ()
;;   "Open vterm in a popper buffer."
;;   (interactive)
;;   (let ((vterm-buffer-name "\*vterm-popup\*")) (vterm)))

;;;###autoload
(defun chaos/unique-eat ()
  "Open eat in a unique buffer."
  (interactive)
  (require 'eat)
  (setq-local eat-buffer-name "*eat*") (eat))

;;;###autoload
(defun chaos/eat-popup ()
  "Open a `shell' in a new window."
  (interactive)
  (setq-local eat-buffer-name "*eat-popup*") (eat))

;;;###autoload
(defun chaos/unique-eshell ()
  "Open eshell in a unique buffer."
  (interactive)
  (setq-local eshell-buffer-name "*eshell*") (eshell))

;;;###autoload
(defun chaos/eshell-popup ()
  "Open eshell in a popper popup."
  (interactive)
  (setq-local eshell-buffer-name "*eshell-popup*") (eshell))

;; (defun chaos/eshell-popup ()
;;   "Open a `shell' in a new window."
;;   (interactive)
;;   (let ((buf (eshell)))
;;     (switch-to-buffer (other-buffer buf))
;;     (switch-to-buffer-other-window buf)))

(provide 'chaos-defs)
;;; chaos-defs.el ends here
