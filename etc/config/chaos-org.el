;;; chaos-org.el --- config for org  -*-no-byte-compile: t; lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;;###autoload
(defun chaos-org-i-dir-h ()
  "Org mode load init hook for loading directories."
  (setq org-directory "~/org/"
        org-agenda-files (list org-directory)
        org-id-locations-file (expand-file-name ".orgids" org-directory)))

;;;###autoload
(defun chaos-org-i-agenda-h ()
  "Org mode load init hook for loading agenda setup."
  (setq org-agenda-window-setup 'current-window
        org-agenda-skip-unavailable-files t
        ;; Shift the agenda to show the previous 3 days and the next 7 days for
        ;; better context on your week. The past is less important than the future.
        org-agenda-span 10
        org-agenda-start-on-weekday nil
        org-agenda-start-day "-3d"
        ;; Optimize `org-agenda' by inhibiting extra work while opening agenda
        ;; buffers in the background. They'll be "restarted" if the user switches to
        ;; them anyway (see `+org-exclude-agenda-buffers-from-workspace-h')
        org-agenda-inhibit-startup t)
  (setq org-agenda-custom-commands
        '(("v" "Nice 💜 "
           ((tags "projets"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'todo
                                               'done))
                   (org-agenda-overriding-header "Projets :")))
            (tags "uni"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'todo
                                               'done
                                               'regexp
                                               ":planning:"))
                   (org-agenda-overriding-header "Université :")))
            (tags "administratif"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'todo
                                               'done))
                   (org-agenda-overriding-header "Administratif ")))
            (tags "perso"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'todo
                                               'done))
                   (org-agenda-overriding-header "Perso ")))
            (agenda "")
            (alltodo ""))))))

;;;###autoload
(defun chaos-org-i-cosmet-h ()
  "Org mode load init hook for loading visual settings."
  (setq org-indirect-buffer-display 'current-window
        org-enforce-todo-dependencies t
        org-fontify-done-headline t
        org-fontify-quote-and-verse-blocks t
        org-fontify-whole-heading-line t
        org-hide-leading-stars t
        org-image-actual-width nil
        org-imenu-depth 6
        org-startup-indented t
        org-tags-column 0
        org-use-sub-superscripts '{}
        ;; `showeverything' is org's default, but it doesn't respect
        ;; `org-hide-block-startup' (#+startup: hideblocks), archive trees,
        ;; hidden drawers, or VISIBILITY properties. `nil' is equivalent, but
        ;; respects these settings.
        org-startup-folded nil)
  (with-no-warnings
    (custom-declare-face '+org-todo-active  '((t (:inherit
                                                  (bold
                                                   font-lock-constant-face
                                                   org-todo)))) "")
    (custom-declare-face '+org-todo-project '((t (:inherit
                                                  (bold
                                                   font-lock-doc-face
                                                   org-todo)))) "")
    (custom-declare-face '+org-todo-onhold  '((t (:inherit
                                                  (bold
                                                   warning
                                                   org-todo)))) "")
    (custom-declare-face '+org-todo-cancel  '((t (:inherit
                                                  (bold
                                                   error
                                                   org-todo)))) ""))
  (setq org-todo-keywords
        '((sequence
           "TODO(t)"  ; A task that needs doing & is ready to do
           "PROJ(p)"  ; A project, which usually contains other tasks
           "LOOP(r)"  ; A recurring task
           "STRT(s)"  ; A task that is in progress
           "WAIT(w)"  ; Something external is holding up this task
           "HOLD(h)"  ; This task is paused/on hold because of me
           "IDEA(i)"  ; An unconfirmed and unapproved task or notion
           "|"
           "DONE(d)"  ; Task successfully completed
           "KILL(k)") ; Task was cancelled, aborted or is no longer applicable
          (sequence
           "[ ](T)"   ; A task that needs doing
           "[-](S)"   ; Task is in progress
           "[?](W)"   ; Task is being held up or paused
           "|"
           "[X](D)")  ; Task was completed
          (sequence "|" "OKAY(o)" "YES(y)" "NO(n)"))
        org-todo-keyword-faces
        '(("[-]"  . +org-todo-active)
          ("STRT" . +org-todo-active)
          ("[?]"  . +org-todo-onhold)
          ("WAIT" . +org-todo-onhold)
          ("HOLD" . +org-todo-onhold)
          ("PROJ" . +org-todo-project)
          ("NO"   . +org-todo-cancel)
          ("KILL" . +org-todo-cancel)))

  ;;"Replace list hyphen with dot"
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 ()
                                  (compose-region (match-beginning 1)
                                                  (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Liberation Sans"
                        :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil
                      :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil
                      :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil
                      :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil
                      :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil
                      :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil
                      :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil
                      :inherit 'fixed-pitch))

(defun chaos-org-i-babel-h ()
  "Org mode load init hook for loading babel settings."
  ;; (org-babel-do-load-languages 'org-babel-load-languages '((emacs-lisp . t)
  ;;                                                          (latex . t)))
  ;; (gnuplot . t)
  ;; (C . t)
  ;; (css . t)
  ;; (shell . t)
  ;; (awk . t)
  ;; (lisp . t)
  ;; (ly . t)
  ;; (lua . t)
  ;; (perl . t)
  ;; (makefile . t)))
  (setq org-src-preserve-indentation t  ; use native major-mode indentation
        org-src-tab-acts-natively t     ; we do this ourselves
        ;; You don't need my permission (just be careful, mkay?)
        org-confirm-babel-evaluate nil
        org-link-elisp-confirm-function nil
        org-src-window-setup 'current-window
        org-babel-lisp-eval-fn #'sly-eval)
  (define-key org-src-mode-map (kbd "C-c C-c") #'org-edit-src-exit)
  ;;doom fix for indent
  (defun +org-fix-newline-and-indent-in-src-blocks-a (&optional indent _arg _interactive)
    "Mimic `newline-and-indent' in src blocks w/ lang-appropriate indentation."
    (when (and indent
               org-src-tab-acts-natively
               (org-in-src-block-p t))
      (save-window-excursion
        (org-babel-do-in-edit-buffer
         (call-interactively #'indent-for-tab-command)))))
  (advice-add #'org-return :after #'+org-fix-newline-and-indent-in-src-blocks-a))

;;;###autoload
(defun chaos-org-i-export-h ()
  "Org mode load init hook for loading ox settings."
  (setq org-export-with-smart-quotes t
        org-html-validation-link nil
        org-latex-prefer-user-labels t))

(use-package ox-pandoc
  :when (executable-find "pandoc")
  :after ox
  :demand t
  :init
  (add-to-list 'org-export-backends 'pandoc)
  (setq org-pandoc-options
        '((standalone . t)
          (mathjax . t)
          (variable . "revealjs-url=https://revealjs.com"))))

(use-package ox-md
  :ensure nil :straight (:type built-in)
  :after ox
  :demand t
  :config
  (add-to-list 'org-export-backends 'md))


(use-package org-crypt
  :ensure nil :straight (:type built-in)
  :after org
  :commands org-encrypt-entries org-encrypt-entry org-decrypt-entries org-decrypt-entry
  :demand t
  :init
  (with-eval-after-load 'org (org-crypt-use-before-save-magic))
  (setq org-tags-exclude-from-inheritance (quote ("crypt")))
  (setq org-crypt-key "80C3 6845 60C8 D993 BB53  472A 6F78 9DBF 419D 55EB"))

(use-package org-clock ; built-in
  :ensure nil  :straight (:type built-in)
  :commands org-clock-save
  :init
  (setq org-clock-persist-file (concat no-littering-var-directory "org-clock-save.el"))
  :config
  (setq org-clock-persist 'history
        org-clock-in-resume t ;; Resume when clocking into task with open clock
        org-clock-out-remove-zero-time-clocks t ;; Remove log if task was clocked for 0:00 (accidental clocking)
        org-clock-history-length 20) ;; The default value (5) is too conservative.
  (add-hook 'kill-emacs-hook #'org-clock-save))

(use-package evil-org
  :hook (org-mode . evil-org-mode)
  :hook (org-capture-mode . evil-insert-state)
  :init
  (defvar evil-org-retain-visual-state-on-shift t)
  (defvar evil-org-special-o/O '(table-row))
  (defvar evil-org-use-additional-insert t)
  :config
  (add-hook 'evil-org-mode-hook #'evil-normalize-keymaps)
  (evil-org-set-key-theme)
  (general-nmap :keymaps '(evil-org-mode-map org-mode-map)
    "zA"  'org-shifttab
    "zc"  'outline-hide-subtree
    "zC"  'outline-hide-subtree
    "zn"  'org-tree-to-indirect-buffer
    "zo"  'outline-show-subtree
    "zO"  'outline-show-subtree
    "zi"  'org-toggle-inline-images)
  (general-imap :keymaps '(evil-org-mode-map org-mode-map)
    "RET" '+org/return
    [return] '+org/return
    "S-RET" '+org/shift-return
    [S-return] '+org/shift-return)
  (general-define-key :keymaps '(evil-org-mode-map org-mode-map) :states '(normal insert)
                      [C-return]   '+org/insert-item-below
                      [C-S-return] '+org/insert-item-above))

(use-package evil-org-agenda
  :ensure nil  :straight (:type built-in)
  :hook (org-agenda-mode . evil-org-agenda-set-keys)
  :config
  (add-to-list 'evil-emacs-state-modes 'org-agenda-mode))

(use-package org
  :straight (org :includes (org-agenda
                            org-capture
                            org-clock
                            org-crypt
                            org-datetree
                            org-protocol
                            ox-publish))
  :after general
  :commands (org-babel-tangle-file)
  :mode ("\\.org\\'" . org-mode)
  :general
  (chaos/leader-def
    "fo" 'chaos/open-org-directory
    "oa" 'org-agenda)
  (chaos/leader-def
    :infix "n"
    "" '(:wk "notes")
    "a" '(org-agenda :wk "Org agenda")
    "f" '(lambda ()
           (interactive)
           (chaos-find-file-recursive-in-dir org-directory) :wk "Find files in notes")
    "l" '(org-store-link :wk "Org store link")
    "m" '(org-tags-view :wk "Tags search")
    "n" '(org-capture :wk "Org capture")
    "N" '(org-capture-goto-target :wk "Goto capture")
    "s" '(lambda ()
           (interactive)
           (consult-ripgrep org-directory) :wk "Search notes")
    "t" '(org-todo-list :wk "Todo list")
    "v" '(org-search-view :wk "View search")
    "y" '(+org/export-to-clipboard :wk "Org export to clipboard"))

  :preface
  (defvar org-modules '(ol-bibtex))
  (chaos-org-i-dir-h)
  (add-hook 'org-mode-hook #'variable-pitch-mode)
  :config
  (chaos-org-i-agenda-h)
  (chaos-org-i-cosmet-h)
  (chaos-org-i-export-h)
  (chaos-org-i-babel-h)
  (setq org-log-done 'time
        org-startup-indented t
        org-archive-subtree-save-file-p t
        org-time-stamp-formats '("<%Y-%m-%d %a %H:%M>" . "<%Y-%m-%d %a %H:%M>")
        org-latex-packages-alist '(("" "babel") ("" "lmodern")))
  (setq org-highlight-latex-and-related '(native script entities)
        org-hide-leading-stars t
        org-hide-emphasis-markers t)

  (setq org-refile-targets '("Archive.org" :maxlevel . 1))
  (advice-add #'org-refile :after #'org-save-all-org-buffers)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;keys
  (setq org-M-RET-may-split-line nil
        ;; insert new headings after current subtree rather than inside it
        org-insert-heading-respect-content t)

  (general-define-key
   :keymaps 'org-mode-map
   "C-c C-i"    'org-toggle-inline-images ;; textmate-esque newline insertion
   "S-RET"      '((lambda (&optional arg)
                    "Insert a literal newline, or dwim in tables.
                     Executes `org-table-copy-down' if in table."
                    (interactive "p")
                    (if (org-at-table-p)
                        (org-table-copy-down arg)
                      (org-return nil arg)))
                  :wk "shift+return"))
  (general-mmap :keymaps 'org-agenda-mode-map
    "C-SPC" 'org-agenda-show-and-scroll-up)
  (chaos/leader-def org-mode-map
    :infix "m"
    "#" 'org-update-statistics-cookies
    "'" 'org-edit-special
    "*" 'org-ctrl-c-star
    "+" 'org-ctrl-c-minus
    "," 'org-switchb
    "." 'org-goto
    "@" 'org-cite-insert
    "." 'consult-org-heading
    "/" 'consult-org-agenda
    "A" 'org-archive-subtree-default
    "e" 'org-export-dispatch
    "f" 'org-footnote-action
    "h" 'org-toggle-heading
    "i" 'org-toggle-item
    "I" 'org-id-get-create
    "k" 'org-babel-remove-result
    "K" '+org/remove-result-blocks
    "n" 'org-store-link
    "o" 'org-set-property
    "q" 'org-set-tags-command
    "t" 'org-todo
    "T" 'org-todo-list
    "x" 'org-toggle-checkbox
    "a" '(:wk "attachments")
    "aa" 'org-attach
    "ad" 'org-attach-delete-one
    "aD" 'org-attach-delete-all
    "af" '+org/find-file-in-attachments
    "al" '+org/attach-file-and-insert-link
    "an" 'org-attach-new
    "ao" 'org-attach-open
    "aO" 'org-attach-open-in-emacs
    "ar" 'org-attach-reveal
    "aR" 'org-attach-reveal-in-emacs
    "au" 'org-attach-url
    "as" 'org-attach-set-directory
    "aS" 'org-attach-sync
    "b" '(:wk "tables")
    "b-" 'org-table-insert-hline
    "ba" 'org-table-align
    "bb" 'org-table-blank-field
    "bc" 'org-table-create-or-convert-from-region
    "be" 'org-table-edit-field
    "bf" 'org-table-edit-formulas
    "bh" 'org-table-field-info
    "bs" 'org-table-sort-lines
    "br" 'org-table-recalculate
    "bR" 'org-table-recalculate-buffer-tables
    "bd" '(:wk "delete")
    "bdc" 'org-table-delete-column
    "bdr" 'org-table-kill-row
    "bi" '(:wk "insert")
    "bic" 'org-table-insert-column
    "bih" 'org-table-insert-hline
    "bir" 'org-table-insert-row
    "biH" 'org-table-hline-and-move
    "bt" '(:wk "toggle")
    "btf" 'org-table-toggle-formula-debugger
    "bto" 'org-table-toggle-coordinate-overlays
    ;; (when (featurep "gnuplot") "bp" 'org-plot/gnuplot)
    "c" '(:wk "clock")
    "cc" 'org-clock-cancel
    "cd" 'org-clock-mark-default-task
    "ce" 'org-clock-modify-effort-estimate
    "cE" 'org-set-effort
    "cg" 'org-clock-goto
    "cG" '((lambda (&rest _) (interactive) (org-clock-goto 'select)) :wk "Clock goto select")
    ;; "cl" '+org/toggle-last-clock
    "ci" 'org-clock-in
    "cI" 'org-clock-in-last
    "co" 'org-clock-out
    "cr" 'org-resolve-clocks
    "cR" 'org-clock-report
    "ct" 'org-evaluate-time-range
    "c=" 'org-clock-timestamps-up
    "c-" 'org-clock-timestamps-down
    "d" '(:wk "date/deadline")
    "dd" 'org-deadline
    "ds" 'org-schedule
    "dt" 'org-time-stamp
    "dT" 'org-time-stamp-inactive
    "g" '(:wk "goto")
    "gg" 'org-goto
    "gg" 'consult-org-heading
    "gG" 'consult-org-agenda
    "gc" 'org-clock-goto
    "gC" '((lambda (&rest _) (interactive) (org-clock-goto 'select)) :wk "Clock goto select")
    "gi" 'org-id-goto
    "gr" 'org-refile-goto-last-stored
    ;; "gv" '((lambda () (interactive) (goto-char (+org-headline-avy))) :wk "Goto visible")
    "gx" 'org-capture-goto-last-stored
    "l" '(:wk "links")
    "lc" 'org-cliplink
    ;; "ld" '+org/remove-link
    "li" 'org-id-store-link
    "ll" 'org-insert-link
    "lL" 'org-insert-all-links
    "ls" 'org-store-link
    "lS" 'org-insert-last-stored-link
    "lt" 'org-toggle-link-display
    "ly" '((lambda ()"Copy the url at point to the clipboard.
    If on top of an Org link, will only copy the link component."
             (interactive)
             (let ((url (thing-at-point 'url)))
               (kill-new (or url (user-error "No URL at point")))
               (message "Copied link: %s" url)))
           :wk "yank link")
    "P" '(:wk "publish")
    "Pa" 'org-publish-all
    "Pf" 'org-publish-current-file
    "Pp" 'org-publish
    "PP" 'org-publish-current-project
    "Ps" 'org-publish-sitemap
    "r" '(:wk "refile")
    ;; "r." '+org/refile-to-current-file
    ;; "rc" '+org/refile-to-running-clock
    ;; "rl" '+org/refile-to-last-location
    ;; "rf" '+org/refile-to-file
    ;; "ro" '+org/refile-to-other-window
    ;; "rO" '+org/refile-to-other-buffer
    ;; "rv" '+org/refile-to-visible
    "rr" 'org-refile
    "rR" 'org-refile-reverse ; to all `org-refile-targets'
    "s" '(:wk "tree/subtree")
    "sa" 'org-toggle-archive-tag
    "sb" 'org-tree-to-indirect-buffer
    "sc" 'org-clone-subtree-with-time-shift
    "sd" 'org-cut-subtree
    "sh" 'org-promote-subtree
    "sj" 'org-move-subtree-down
    "sk" 'org-move-subtree-up
    "sl" 'org-demote-subtree
    "sn" 'org-narrow-to-subtree
    "sr" 'org-refile
    "ss" 'org-sparse-tree
    "sA" 'org-archive-subtree-default
    "sN" 'widen
    "sS" 'org-sort
    "p" '(:wk "priority")
    "pd" 'org-priority-down
    "pp" 'org-priority
    "pu" 'org-priority-up
    "m" 'org-edit-src-code))

(provide 'chaos-org)
;;; chaos-org.el ends here
