;;; chaos-modeline.el --- my modeline config   -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


(column-number-mode 1)

(defun chaos-mode-line-fill (reserve)
  "Return empty space using FACE and leaving RESERVE space on the right."
  (when (< emacs-major-version 30)
    (when (and window-system (eq 'right (get-scroll-bar-mode)))
      (setq reserve (- reserve 3)))
    (propertize " "
                'display
                `((space :align-to (- (+ right right-fringe right-margin) ,reserve))))))

(setq-default mode-line-position-column-line-format '("%l:%c"))
(setq-default mode-line-format
              '("%e" (:eval (propertize " %I "))
                ;; buffer id
                (:eval
                 (propertize "%b" 'face (cond ((buffer-modified-p)
                                               '(error bold mode-line-buffer-id))
                                              ('t  'mode-line-buffer-id))
                             'help-echo  (buffer-name)))
                ;; RO !
                (buffer-read-only (:propertize " RO" face warning))
                "  "
                mode-line-position-column-line-format
                "  "
                mode-line-percent-position
                ;; visual selection
                (:eval
                 (when (or (and (bound-and-true-p evil-local-mode)
                                (eq evil-state 'visual))
                           mark-active)
                   (cl-destructuring-bind (beg . end)
                       (if (bound-and-true-p evil-visual-selection)
                           (cons evil-visual-beginning evil-visual-end)
                         (cons (region-beginning) (region-end)))
                     (propertize
                      (let ((lines (count-lines beg (min end (point-max)))))
                        (concat " "
                                (cond
                                 ((or (bound-and-true-p rectangle-mark-mode)
                                      (and (bound-and-true-p evil-visual-selection)
                                           (eq 'block evil-visual-selection)))
                                  (let ((cols (abs (- (+modeline--column end)
                                                      (+modeline--column beg)))))
                                    (format "%dx%dB" lines cols)))
                                 ((and (bound-and-true-p evil-visual-selection)
                                       (eq evil-visual-selection 'line))
                                  (format "%dL" lines))
                                 ((> lines 1)
                                  (format "%dC %dL" (- end beg) lines))
                                 ((format "%dC" (- end beg))))
                                (when (derived-mode-p 'text-mode)
                                  (format " %dW" (count-words beg end))) " "))
                      'face  'success))))
                (:eval (chaos-mode-line-fill 40)) ;; TODO this will be superfluous on emacs 30
                mode-line-format-right-align
                mode-line-misc-info
                (""
                 (:propertize mode-name face bold)
                 mode-line-process "%n" " ");;major mode
                (vc-mode (""vc-mode " "))
                (flymake-mode ("" flymake-mode-line-counters))))

(provide 'chaos-modeline)
;;; chaos-modeline.el ends here
