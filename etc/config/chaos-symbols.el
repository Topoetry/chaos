;;; chaos-symbols.el --- ligatures config            -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;; example
;; (defvar +ligatures-extra-symbols
;;   '(;; org
;;     :name          "»"
;;     :src_block     "»"
;;     :src_block_end "«"
;;     :quote         "“"
;;     :quote_end     "”"
;;     ;; Functional
;;     :lambda        "λ"
;;     :def           "ƒ"
;;     :composition   "∘"
;;     :map           "↦"
;;     ;; Types
;;     :null          "∅"
;;     :true          "𝕋"
;;     :false         "𝔽"
;;     :int           "ℤ"
;;     :float         "ℝ"
;;     :str           "𝕊"
;;     :bool          "𝔹"
;;     :list          "𝕃"
;;     ;; Flow
;;     :not           "￢"
;;     :in            "∈"
;;     :not-in        "∉"
;;     :and           "∧"
;;     :or            "∨"
;;     :for           "∀"
;;     :some          "∃"
;;     :return        "⟼"
;;     :yield         "⟻"
;;     ;; Other
;;     :union         "⋃"
;;     :intersect     "∩"
;;     :diff          "∖"
;;     :tuple         "⨂"
;;     :pipe          "" ;; FIXME: find a non-private char
;;     :dot           "•")
;;   "Maps identifiers to symbols, recognized by `set-ligatures'.

;; This should not contain any symbols from the Unicode Private Area! There is no
;; universal way of getting the correct symbol as that area varies from font to
;; font.")

(defvar +ligatures-extra-alist '((t))
  "A map of major modes to symbol lists (for `prettify-symbols-alist').")

(defvar +ligatures-extras-in-modes t
  "List of major modes where extra ligatures should be enabled.
If t, enable it everywhere (except `fundamental-mode').
If the first element is not, enable it in any mode besides what is listed.
If nil, don't enable these extra ligatures anywhere (though it's more
efficient to remove the `+extra' flag from the :ui ligatures module instead).")

(defun +ligatures--enable-p (modes)
  "Return t if ligatures should be enabled in this buffer depending on MODES."
  (unless (eq major-mode 'fundamental-mode)
    (or (eq modes t)
        (if (eq (car modes) 'not)
            (not (apply #'derived-mode-p (cdr modes)))
          (apply #'derived-mode-p modes)))))

(defun +ligatures-init-extra-symbols-h ()
  "Set up `prettify-symbols-mode' for the current buffer.

Overwrites `prettify-symbols-alist' and activates `prettify-symbols-mode' if
\(and only if) there is an associated entry for the current major mode (or a
parent mode) in `+ligatures-extra-alist' AND the current mode (or a parent mode)
isn't disabled in `+ligatures-extras-in-modes'."
  (when after-init-time
    (when-let*
        (((+ligatures--enable-p +ligatures-extras-in-modes))
         (symbols
          (if-let* ((symbols (assq major-mode +ligatures-extra-alist)))
              (cdr symbols)
            (cl-loop for (mode . symbols) in +ligatures-extra-alist
                     if (derived-mode-p mode)
                     return symbols))))
      (setq prettify-symbols-alist
            (append symbols
                    ;; Don't overwrite global defaults
                    (default-value 'prettify-symbols-alist)))
      (when (bound-and-true-p prettify-symbols-mode)
        (prettify-symbols-mode -1))
      (prettify-symbols-mode +1))))


(provide 'chaos-symbols)
;;; chaos-symbols.el ends here
