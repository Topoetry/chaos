;;; chaos-spell.el --- spell check extracted from doom  -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Extracted spell checking from doom

;;; Code:
(require 'general)

;;;###autoload
(defun +spell--correct (replace poss word orig-pt start end)
  (cond ((eq replace 'ignore)
         (goto-char orig-pt)
         nil)
        ((eq replace 'save)
         (goto-char orig-pt)
         (ispell-send-string (concat "*" word "\n"))
         (ispell-send-string "#\n")
         (setq ispell-pdict-modified-p '(t)))
        ((or (eq replace 'buffer) (eq replace 'session))
         (ispell-send-string (concat "@" word "\n"))
         (add-to-list 'ispell-buffer-session-localwords word)
         (or ispell-buffer-local-name ; session localwords might conflict
             (setq ispell-buffer-local-name (buffer-name)))
         (if (null ispell-pdict-modified-p)
             (setq ispell-pdict-modified-p
                   (list ispell-pdict-modified-p)))
         (goto-char orig-pt)
         (if (eq replace 'buffer)
             (ispell-add-per-file-word-list word)))
        (replace
         (let ((new-word (if (atom replace)
                             replace
                           (car replace)))
               (orig-pt (+ (- (length word) (- end start))
                           orig-pt)))
           (unless (equal new-word (car poss))
             (delete-region start end)
             (goto-char start)
             (insert new-word))))
        ((goto-char orig-pt)
         nil)))


;;;###autoload
(defun +spell-correct-generic-fn (candidates word)
  (completing-read (format "Corrections for %S: " word) candidates))

;;;###autoload
(defun +spell/correct ()
  "Correct spelling of word at point."
  (interactive)
  ;; spell-fu fails to initialize correctly if it can't find aspell or a similar
  ;; program. We want to signal the error, not tell the user that every word is
  ;; spelled correctly.
  (unless (;; This is what spell-fu uses to check for the aspell executable
           or (and ispell-really-aspell ispell-program-name)
           (executable-find "aspell"))
    (user-error "Aspell is required for spell checking"))

  (ispell-set-spellchecker-params)
  (save-current-buffer
    (ispell-accept-buffer-local-defs))
  (cl-destructuring-bind (start . end)
      (or (bounds-of-thing-at-point 'word)
          (user-error "No word at point"))
    (let ((word (thing-at-point 'word t))
          (orig-pt (point))
          poss ispell-filter)
      (ispell-send-string "%\n")
      (ispell-send-string (concat "^" word "\n"))
      (while (progn (accept-process-output ispell-process)
                    (not (string= "" (car ispell-filter)))))
      ;; Remove leading empty element
      (setq ispell-filter (cdr ispell-filter))
      ;; ispell process should return something after word is sent. Tag word as
      ;; valid (i.e., skip) otherwise
      (unless ispell-filter
        (setq ispell-filter '(*)))
      (when (consp ispell-filter)
        (setq poss (ispell-parse-output (car ispell-filter))))
      (cond
       ((or (eq poss t) (stringp poss))
        ;; don't correct word
        (message "%s is correct" (funcall ispell-format-word-function word))
        t)
       ((null poss)
        ;; ispell error
        (error "Ispell: error in Ispell process"))
       (t
        ;; The word is incorrect, we have to propose a replacement.
        (setq res (funcall +spell-correct-interface (nth 2 poss) word))
        ;; Some interfaces actually eat 'C-g' so it's impossible to stop rapid
        ;; mode. So when interface returns nil we treat it as a stop.
        (unless res (setq res (cons 'break word)))
        (cond
         ((stringp res)
          (+spell--correct res poss word orig-pt start end))
         ((let ((cmd (car res))
                (wrd (cdr res)))
            (unless (or (eq cmd 'skip)
                        (eq cmd 'break)
                        (eq cmd 'stop))
              (+spell--correct cmd poss wrd orig-pt start end)
              (unless (string-equal wrd word)
                (+spell--correct wrd poss word orig-pt start end))))))
        (ispell-pdict-save t))))))


(use-package ispell
  :ensure nil ;; :straight nil ; TODO
  :after spell-fu
  :defer t
  :config
  (dolist
      (result
       (list
        '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:")
        '("#\\+BEGIN_SRC" . "#\\+END_SRC")
        '("#\\+BEGIN_EXAMPLE" . "#\\+END_EXAMPLE"))
       (with-no-warnings ispell-skip-region-alist))
    (cl-pushnew result ispell-skip-region-alist :test
                (function equal)))

  ;; set only aspell
  (setq ispell-program-name "aspell"
        ispell-extra-args '("--sug-mode=ultra"
                            "--run-together"))

  (unless ispell-aspell-dict-dir
    (setq ispell-aspell-dict-dir
          (ispell-get-aspell-config-value "dict-dir")))
  (unless ispell-aspell-data-dir
    (setq ispell-aspell-data-dir
          (ispell-get-aspell-config-value "data-dir")))
  (unless ispell-personal-dictionary
    (setq ispell-personal-dictionary
          (expand-file-name (concat "ispell/" ispell-dictionary ".pws")
                            no-littering-etc-directory)))

  (add-hook 'text-mode-hook
            (defun +spell-remove-run-together-switch-for-aspell-h ()
              (setq-local ispell-extra-args (remove "--run-together" ispell-extra-args))))

  (defun +spell-init-ispell-extra-args-a (orig-fun &rest args)
    :around '(ispell-word flyspell-auto-correct-word)
    (let ((ispell-extra-args (remove "--run-together" ispell-extra-args)))
      (ispell-kill-ispell t)
      (apply orig-fun args)
      (ispell-kill-ispell t)))
  (if (executable-find ispell-program-name)
      (ispell-check-version)
    (warn "Can't find %s in your $PATH" ispell-program-name)))

(use-package spell-fu
  :when (executable-find "aspell")
  :defer t
  :hook ((yaml-mode conf-mode prog-mode text-mode) . spell-fu-mode)
  :general
  ([remap ispell-word] #'+spell/correct)
  (chaos/leader-def
    "ts" '(spell-fu-mode :wk "Spell checker"))
  (general-nmap
    "zg" 'spell-fu-word-add
    "zw" 'spell-fu-word-remove
    "[s" 'spell-fu-goto-previous-error
    "]s" 'spell-fu-goto-next-error
    "és" 'spell-fu-goto-previous-error
    "ès" 'spell-fu-goto-next-error)
  :preface
  (defvar +spell-correct-interface #'+spell-correct-generic-fn
    "Function to use to display corrections.")
  :init
  (defvar +spell-excluded-faces-alist
    '((markdown-mode . (markdown-code-face
                        markdown-html-attr-name-face
                        markdown-html-attr-value-face
                        markdown-html-tag-name-face
                        markdown-inline-code-face
                        markdown-link-face
                        markdown-markup-face
                        markdown-plain-url-face
                        markdown-reference-face
                        markdown-url-face))
      (org-mode . (org-block
                   org-block-begin-line
                   org-block-end-line
                   org-cite
                   org-cite-key
                   org-code
                   org-date
                   org-footnote
                   org-formula
                   org-inline-src-block
                   org-latex-and-related
                   org-link
                   org-meta-line
                   org-property-value
                   org-ref-cite-face
                   org-special-keyword
                   org-tag
                   org-todo
                   org-todo-keyword-done
                   org-todo-keyword-habt
                   org-todo-keyword-kill
                   org-todo-keyword-outd
                   org-todo-keyword-todo
                   org-todo-keyword-wait
                   org-verbatim))
      (latex-mode . (font-latex-math-face
                     font-latex-sedate-face
                     font-lock-function-name-face
                     font-lock-keyword-face
                     font-lock-variable-name-face)))
    "Faces in certain major modes that spell-fu will not spellcheck.")

  (setq spell-fu-directory (concat no-littering-etc-directory "spell-fu"))
  :config
  (add-hook 'spell-fu-mode-hook
            (defun +spell-init-excluded-faces-h ()
              "Set `spell-fu-faces-exclude' according to `+spell-excluded-faces-alist'."
              (when-let (excluded (cdr (cl-find-if #'derived-mode-p +spell-excluded-faces-alist :key #'car)))
                (setq-local spell-fu-faces-exclude excluded)))))

(provide 'chaos-spell)
;;; chaos-spell.el ends here
