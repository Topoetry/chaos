#!/usr/bin/env sh

exec emacs -q --script "$1"/etc/bootstrap/bootstrap.el "$1"
