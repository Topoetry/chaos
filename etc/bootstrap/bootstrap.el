;;; bootstrap.el --- bootsrap config -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2025 Léo, chaos given form
;;
;; Author: Léo, chaos given form <leo.tisane@disroot.org>
;; Maintainer: Léo, chaos given form <leo.tisane@disroot.org>
;; Created: February 17, 2025
;; Modified: February 17, 2025
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/leo/bootstrap
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Call as emacs -Q --script $user-emacs-directory
;;
;;; Code:
(require 'ob-tangle)
(if (file-exists-p (concat (elt argv 0) "init.org"))
    (org-babel-tangle-file
     (concat (elt argv 0) "init.org")
     (concat (elt argv 0) "init.el"))
  (error "init.org not found !"))

(provide 'bootstrap)
;;; bootstrap.el ends here
