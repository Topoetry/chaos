;;; early-init.el --- early init                     -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Léo, chaos given form

;; Author: Léo, chaos given form <leo@Tisane-l33t>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(prefer-coding-system 'utf-8)
;; tangle my config with another emacs process if it doesn't exist
(unless (file-exists-p (concat user-emacs-directory "init.el"))
  (call-process (executable-find "emacs") nil nil nil "-q" "--script"
                (concat user-emacs-directory "etc/bootstrap/bootstrap.el")
                user-emacs-directory))

(when (and (fboundp 'startup-redirect-eln-cache)
           (fboundp 'native-comp-available-p)
           (native-comp-available-p))
  (startup-redirect-eln-cache
   (convert-standard-filename
    (concat user-emacs-directory "var/eln-cache/"))))

(setq package-enable-at-startup nil)
(setq inhibit-startup-message t
      inhibit-splash-screen t) ;; no thanks
(setq gc-cons-threshold (* 50 1000 1000)) ;; big garbage for big booting


(provide 'early-init)
;;; early-init.el ends here
